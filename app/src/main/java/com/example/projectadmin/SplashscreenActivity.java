package com.example.projectadmin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class SplashscreenActivity extends AppCompatActivity {
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        imageView = findViewById(R.id.logo);

        final Intent login = new Intent(this, LoginActivity.class);

//        Animation anime = AnimationUtils.loadAnimation(this, R.anim.transition);
//        imageView.startAnimation(anime);

        // Delay period before moving to next activity
        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(2500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    startActivity(login);
                    finish();
                }
            }
        };
        timer.start();
    }
}
