package com.example.projectadmin.ui.map;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.example.projectadmin.MainActivity;
import com.example.projectadmin.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.PolyUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static androidx.core.content.ContextCompat.getSystemService;

public class MapFragment extends Fragment implements OnMapReadyCallback, View.OnClickListener, RoutingListener, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private GoogleMap mMap;
    private GoogleApiClient googleApiClient;
    private Location myLocation;
    private LocationManager mLocationManager, locationManager;
    private LocationRequest mLocationRequest;
    private com.google.android.gms.location.LocationListener mLocationListener;
    private Polygon mPolygon = null;
    ArrayList<PolygonOptions> mAllPolygons = new ArrayList<PolygonOptions>();
    private DatabaseReference mDatabaseReference;
    private ArrayList<LatLng> sensors = new ArrayList<>();
    private Location currLocation;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private FloatingActionButton mStyle, mLocation, mDirection, mClear;
    private ArrayList keys  = new ArrayList();
    private ArrayList keys1  = new ArrayList();
    private int geofenceLocation = 0;
    private int geofenceLocation1 = 0;
    private List<Polyline> polylines;
    private int markerLocation = 0;
    private static final int[] COLORS = new int[]{R.color.colorPrimaryDark, R.color.colorPrimary, R.color.yellowDark, R.color.colorAccent,R.color.primary_dark_material_light};

    private static final int REQUEST_CODE = 101;
    private static final int DEFAULT_ZOOM = 17;
    private long UPDATE_INTERVAL = 200;
    private long FASTEST_INTERVAL = 500;
    private int[] style = {R.raw.style_night, R.raw.style_standard};
    private int currentStyle = 0;

    public MapFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        mapViewModel = ViewModelProviders.of(this).get(GeofenceViewModel.class);
        View root = inflater.inflate(R.layout.fragment_map, container, false);
//        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        mClear = root.findViewById(R.id.clear_route);
        mClear.setOnClickListener(this);
        mStyle = root.findViewById(R.id.style);
        mStyle.setOnClickListener(this);
        mLocation = root.findViewById(R.id.myLocationButton);
        mLocation.setOnClickListener(this);
        mDirection = root.findViewById(R.id.direction);
        mDirection.setOnClickListener(this);

        polylines = new ArrayList<>();
//        latLngList = new ArrayList<>();
//        markerLngList = new ArrayList<>();

        googleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationManager = (LocationManager) getActivity().getSystemService(getActivity().LOCATION_SERVICE);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mDatabaseReference = FirebaseDatabase.getInstance().getReference("database");
        getLastLocation();
        return root;
    }

    private void getLastLocation() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            return;
        }

        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currLocation = location;
                    SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

                    assert mapFragment != null;
                    mapFragment.getMapAsync(MapFragment.this);
                    startLocationUpdates();
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//                mMap.clear(); //clear old markers

        LatLng latLng = new LatLng(currLocation.getLatitude(), currLocation.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, DEFAULT_ZOOM));
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setRotateGesturesEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setTiltGesturesEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);

        LatLng sensor1 = new LatLng(-1.309465, 36.8122377);
        sensors.add(sensor1);

//        f+
        displayAddedGeofences();
        LatLng userLocation = new LatLng(currLocation.getLatitude(), currLocation.getLongitude());
        geofenceListener(userLocation);
        Log.d("MALLPOLYGON READY DATA", String.valueOf(mAllPolygons));
        getMarkers();
        startLocationUpdates();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.clear_route:
                for (Polyline polyline : polylines){
                    polyline.remove();
                }
                polylines.clear();

                mClear.hide();
                break;

            case R.id.style:
                if (currentStyle == style.length && style.length != 0) {
                    currentStyle = 0;
                }

                try {
                    mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), style[currentStyle++]));

                } catch (Resources.NotFoundException e) {
                    Log.e("MapsActivity", "Cannot find style.", e);
                }
                break;

            case R.id.direction:
                calcDistance();
                break;

            case R.id.myLocationButton:
                LatLng latLng = new LatLng(currLocation.getLatitude(), currLocation.getLongitude());
                mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                break;
        }
    }

    private void displayAddedGeofences(){
        mDatabaseReference.child("geofences").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {

                    keys.add(postSnapshot.getKey());
                    Log.d("FETCHED DATA",keys.toString());
                    Log.d("FETCHED DATA",postSnapshot.toString());
                    mDatabaseReference.child("geofences").child(String.valueOf(keys.get(geofenceLocation))).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                            ArrayList<LatLng> latLngList = new ArrayList<>();
                            ArrayList<LatLng> markerLngList = new ArrayList<>();
                            Log.d("GOTTEN DATA", String.valueOf(dataSnapshot1.getRef()));
                            for (DataSnapshot postSnapshot1 : dataSnapshot1.getChildren()){

                                Log.d("GOTTEN DATA",postSnapshot1.getKey());
                                Log.d("GOTTEN DATA", String.valueOf(postSnapshot1.getValue()));
                                Log.d("GOTTEN DATA", String.valueOf(postSnapshot1.child("latitude").getValue()));
                                Log.d("GOTTEN DATA", String.valueOf(postSnapshot1.child("longitude").getValue()));
                                LatLng latLng = new LatLng(Double.parseDouble(postSnapshot1.child("latitude").getValue().toString()), Double.parseDouble(postSnapshot1.child("longitude").getValue().toString()));
                                latLngList.add(latLng);

                                if (String.valueOf(postSnapshot1.getKey()).equals("0")) {

                                    LatLng markerLng = new LatLng(Double.parseDouble(postSnapshot1.child("latitude").getValue().toString()), Double.parseDouble(postSnapshot1.child("longitude").getValue().toString()));
                                    markerLngList.add(markerLng);
                                    Log.d("MARKER DATA", String.valueOf(markerLngList));
                                }

                            }
                            Log.d("CONVERTED DATA", String.valueOf(dataSnapshot1.getRef()));
                            Log.d("CONVERTED DATA", String.valueOf(latLngList));

                            PolygonOptions polygonOptions = new PolygonOptions()
                                    .addAll(latLngList)
                                    .clickable(true);

                            try {
                                mPolygon = mMap.addPolygon(polygonOptions);
                                mPolygon.setStrokeColor(Color.TRANSPARENT);
                                mPolygon.setClickable(true);
                                mPolygon.isClickable();
                            } catch (Exception e){
                                Log.e("latlngException", e.getMessage());
                            }
                        }


                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    Log.d("MALLPOLYGON DATA", String.valueOf(mAllPolygons));
                    geofenceLocation ++;

                }
                keys.clear();
                geofenceLocation = 0;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("FETCHED DATA", "loadPost:onCancelled", databaseError.toException());
            }
        });
    }

    public void geofenceListener(final LatLng userLocation){
        mDatabaseReference.child("geofences").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {

                    keys.add(postSnapshot.getKey());
                    mDatabaseReference.child("geofences").child(String.valueOf(keys.get(geofenceLocation))).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                            ArrayList<LatLng> latLngList = new ArrayList<>();
                            ArrayList<LatLng> markerLngList = new ArrayList<>();
                            for (DataSnapshot postSnapshot1 : dataSnapshot1.getChildren()){
                                LatLng latLng = new LatLng(Double.parseDouble(postSnapshot1.child("latitude").getValue().toString()), Double.parseDouble(postSnapshot1.child("longitude").getValue().toString()));
                                latLngList.add(latLng);
                                if (String.valueOf(postSnapshot1.getKey()).equals("0")) {
                                    LatLng markerLng = new LatLng(Double.parseDouble(postSnapshot1.child("latitude").getValue().toString()), Double.parseDouble(postSnapshot1.child("longitude").getValue().toString()));
                                    markerLngList.add(markerLng);
                                }
                            }
                            String[] splitRef = String.valueOf(dataSnapshot1.getRef()).split("/");
                            PolyUtil.containsLocation(userLocation, latLngList, false);
                            if (PolyUtil.containsLocation(userLocation, latLngList, false)){
                                int count = 0;
                                for (int i = 0; i < sensors.size(); i ++){
                                    if (PolyUtil.containsLocation(sensors.get(i), latLngList, false)){
                                        count ++;
                                    }
                                }

                                if (count > 0) {
                                    String notification = "There are available parking spots in this location. Click on the Direction button to be directed to the nearest one.";
                                    setNotification(notification);
                                    Log.w("USERLOCATION DATA", "You are in: " + splitRef[(splitRef.length - 1)]);
                                } else {
                                    String notification = "There are no available parking spots in this area.";
                                    setNotification(notification);
                                    Log.w("USERLOCATION DATA", "You are in: " + splitRef[(splitRef.length - 1)]);
                                }
                            }
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                    geofenceLocation ++;
                }
                keys.clear();
                geofenceLocation = 0;
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("FETCHED DATA", "loadPost:onCancelled", databaseError.toException());
            }
        });
    }

    public void setNotification(String s){
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getActivity(), "1");
                mBuilder.setSmallIcon(R.drawable.ic_logo_icon_orange)
                    .setContentTitle("SMPark")
                    .setContentText(s)
                    .setStyle(new NotificationCompat.BigTextStyle()
                                                    .bigText(s))
                    .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setCategory(Notification.CATEGORY_REMINDER);

        try {
            NotificationManager notificationManager = ( NotificationManager ) getActivity().getSystemService( getActivity().NOTIFICATION_SERVICE );
            notificationManager.notify(1, mBuilder.build());
        } catch (Exception e){
            Log.e("notificationException", e.getMessage());
        }
//
//        // When you issue multiple notifications about the same type of event,
//        // it’s best practice for your app to try to update an existing notification
//        // with this new information, rather than immediately creating a new notification.
//        // If you want to update this notification at a later date, you need to assign it an ID.
//        // You can then use this ID whenever you issue a subsequent notification.
//        // If the previous notification is still visible, the system will update this existing notification,
//        // rather than create a new one. In this example, the notification’s ID is 001//
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Smpark";
            String description = "chanel for notification";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel("1", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getActivity().getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public Bitmap smallerMarker(){
        int height = 100;
        int width = 100;
        BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.mipmap.ic_parking_foreground);
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
        return smallMarker;
    }

    private void getMarkers(){
        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference ref = firebaseDatabase.getReference("sensors");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    final MapModel map = new MapModel();
                    keys1.add(postSnapshot.getKey());

                    ref.child("sensor1").addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                            Log.d("MapActivity", String.valueOf(dataSnapshot1.child("Latitude").getValue()));
                            Log.d("MapActivity", String.valueOf(dataSnapshot1.child("Longitude").getValue()));
                            Log.d("MapActivity", String.valueOf(dataSnapshot1.child("Status").getValue()));

                            map.setLatitude(String.valueOf(dataSnapshot1.child("Latitude").getValue()));
                            map.setLongitude(String.valueOf(dataSnapshot1.child("Longitude").getValue()));
                            map.setStatus(Boolean.parseBoolean(String.valueOf(dataSnapshot1.child("Status").getValue())));

                            try {
                                if (map.isStatus()){
                                    LatLng latLng = new LatLng(Double.parseDouble(map.getLatitude()), Double.parseDouble(map.getLongitude()));
                                    int height = 100;
                                    int width = 100;
                                    BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.mipmap.ic_parking_foreground);
                                    Bitmap b = bitmapdraw.getBitmap();
                                    Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                                    mMap.addMarker(new MarkerOptions()
                                            .position(latLng)
                                            .icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
                                    sensors.add(latLng);
                                }
                                geofenceLocation1 ++;
                            } catch (Exception e){

                                Log.e("getmarker", e.getMessage());

                                if (map.isStatus()){
                                    LatLng latLng = new LatLng(Double.parseDouble(map.getLatitude()), Double.parseDouble(map.getLongitude()));
                                    int height = 100;
                                    int width = 100;
                                    BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.mipmap.ic_parking_foreground);
                                    Bitmap b = bitmapdraw.getBitmap();
                                    Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);
                                    mMap.addMarker(new MarkerOptions()
                                            .position(latLng)
                                            .icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
                                    sensors.add(latLng);
                                }
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }

                    });
                }
                geofenceLocation1 = 0;

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                // Failed to read value
                Log.e("MapActivity", "Failed to read value.", databaseError.toException());
            }
        });
        Log.d("MapActivity", sensors.toString());
    }

    public void calcDistance (){
        if (sensors.size() == 0){
            Toast.makeText(getActivity(), "There are no available parking spots", Toast.LENGTH_LONG).show();
        } else {
            HashMap<Integer, Double> distance = new HashMap<>();

            Location location1 = new Location("");
            location1.setLatitude(currLocation.getLatitude());
            location1.setLongitude(currLocation.getLongitude());

            for (int i = 0; i < sensors.size(); i ++){
                Location location2 = new Location("");
                location2.setLatitude(sensors.get(i).latitude);
                location2.setLongitude(sensors.get(i).longitude);

                double dis = location1.distanceTo(location2);
                Log.d("DISTANCE", String.valueOf(dis));

                distance.put(i, dis);
            }
            double minDis = Collections.min(distance.values());
            int key = 0;

            if (minDis > 1000){
                Toast.makeText(getActivity(), "There are no parking spots close to your location", Toast.LENGTH_LONG).show();
            } else{
                for (Map.Entry<Integer, Double> entry : distance.entrySet()) {
                    if (entry.getValue().equals(minDis)) {
                        key = entry.getKey();
                    }
                }


                Log.d("DISTANCE", sensors.get(key) + " has the shortest distance");
                LatLng latLng = new LatLng(currLocation.getLatitude(), currLocation.getLongitude());

                Log.d("DISTANCE", "From " + latLng + " to " + sensors.get(key));

                getRoute(sensors.get(key));
                mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
                mClear.show();
            }
        }

    }

    private void getRoute(LatLng des) {
        LatLng latLng = new LatLng(currLocation.getLatitude(), currLocation.getLongitude());

        Routing routing = new Routing.Builder()
                .key("AIzaSyCBHdkX44gzYFr0_sbepYJK5rFVxS11GeY")
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(true)
                .waypoints(latLng, des)
                .build();
        routing.execute();
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        // The Routing request failed
        if(e != null) {
            Toast.makeText(getActivity(), "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
            Log.e("ERROR", "Error: " + e.getMessage());
        }else {
            Toast.makeText(getActivity(), "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        if(polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines = new ArrayList<>();
        //add route(s) to the map.
        for (int i = 0; i <route.size(); i++) {

            //In case of more than 5 alternative routes
            int colorIndex = i % COLORS.length;

            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(Color.rgb(0, 132, 248));
            polyOptions.width(15);
            polyOptions.addAll(route.get(i).getPoints());
            Polyline polyline = mMap.addPolyline(polyOptions);
            polylines.add(polyline);
        }
    }

    @Override
    public void onRoutingCancelled() {

    }

    @Override
    public void onLocationChanged(Location location) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        Toast.makeText(getActivity(), "Updated location: " + location.getLatitude() + location.getLongitude(), Toast.LENGTH_LONG).show();
        Log.d("locationChange", "Updated location: " + location.getLatitude() + location.getLongitude());
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        startLocationUpdates();
        geofenceListener(latLng);

        SupportMapFragment supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        assert supportMapFragment != null;
        supportMapFragment.getMapAsync(MapFragment.this);
    }

    private void startLocationUpdates(){
        mLocationRequest = LocationRequest.create()
                                        .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                                        .setInterval(UPDATE_INTERVAL)
                                        .setFastestInterval(FASTEST_INTERVAL);

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

//        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, (android.location.LocationListener) locationManager);
//        myLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//        if (myLocation != null) {
//            LatLng latLng = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
//            geofenceListener(latLng);
//        }
//        Task<Location> task = fusedLocationProviderClient.requestLocationUpdates(googleApiClient, mLocationRequest, this);
//        task.addOnSuccessListener(new OnSuccessListener<Location>() {
//            @Override
//            public void onSuccess(Location location) {
//                if (location != null) {
//                    currLocation = location;
//                    SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
//
//                    assert mapFragment != null;
//                    mapFragment.getMapAsync(MapFragment.this);
//                    startLocationUpdates();
//                }
//            }
//        });

//        LatLng latLng = new LatLng(currLocation.getLatitude(), currLocation.getLongitude());
//        geofenceListener(latLng);
//        googleApiClient.connect();
//        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, mLocationRequest, this);
    }

    @Override
    public void onStart() {
        super.onStart();
        startLocationUpdates();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    public void onStop() {
        super.onStop();

        if (googleApiClient.isConnected()){
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        startLocationUpdates();
        myLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    startLocationUpdates();
                }
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}