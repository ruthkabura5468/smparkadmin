package com.example.projectadmin.ui.geofence;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class GeofenceViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public GeofenceViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is sensors fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}