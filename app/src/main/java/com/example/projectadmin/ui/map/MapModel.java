package com.example.projectadmin.ui.map;

import androidx.annotation.NonNull;

import com.google.firebase.database.IgnoreExtraProperties;

//@IgnoreExtraProperties
public class MapModel {
    public String latitude;
    public String longitude;
    public boolean status;

    public MapModel() {
    }

    public MapModel(String latitude, String longitude, boolean status) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.status = status;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
