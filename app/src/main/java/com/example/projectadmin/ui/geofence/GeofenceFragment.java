package com.example.projectadmin.ui.geofence;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;

import com.example.projectadmin.R;
import com.example.projectadmin.model.LatLngModel;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;

public class GeofenceFragment extends Fragment implements OnMapReadyCallback, View.OnClickListener, GeoQueryEventListener, GoogleMap.OnPolygonClickListener, GoogleMap.OnPolylineClickListener {
    GeofencingClient geofencingClient;
    private Dialog mDialogSave, mDialogDelete;
    private FloatingActionButton mAdd, mRemove, mMapType, mClear;
    private TextInputEditText geofenceName, promptGeofenceName;
    private CircularProgressButton cancel, save, mCancel, mDelete;
    private GoogleMap mMap;
    private Polygon mPolygon = null;
    private List<LatLng> mLatLngs = new ArrayList<>();
    private List<Marker> mMarkers = new ArrayList<>();
//    private ArrayList<LatLng> latLngList = new ArrayList<>();
//    private ArrayList<LatLng> markerLngList = new ArrayList<>();
    private DatabaseReference mDatabaseReference;
    private GeoFire mGeoFire;
    private List<LatLng> geofences;
    private ArrayList keys  = new ArrayList();
    private int geofenceLocation = 0;
    public GeofenceFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        geofenceViewModel = ViewModelProviders.of(this).get(GeofenceViewModel.class);
        View root = inflater.inflate(R.layout.fragment_geofence, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.geofence);

        mDialogSave = new Dialog(Objects.requireNonNull(getActivity()));
        mDialogSave.setContentView(R.layout.prompt_save);
        mDialogDelete = new Dialog(getActivity());
        mDialogDelete.setContentView(R.layout.prompt_delete);

        mAdd = root.findViewById(R.id.addLocation);
        mAdd.setOnClickListener(this);
        mRemove = root.findViewById(R.id.deleteLocation);
        mRemove.setOnClickListener(this);
        mClear = root.findViewById(R.id.clear_map);
        mClear.setOnClickListener(this);
        mMapType = root.findViewById(R.id.map_type);
        mMapType.setOnClickListener(this);

        geofenceName = mDialogSave.findViewById(R.id.geofence_name);
        cancel = mDialogSave.findViewById(R.id.cancel);
        save = mDialogSave.findViewById(R.id.save);

        promptGeofenceName = mDialogDelete.findViewById(R.id.prompt_geofence_name);
        mCancel = mDialogDelete.findViewById(R.id.prompt_cancel);
        mDelete = mDialogDelete.findViewById(R.id.prompt_delete);

//        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
//        mDatabaseReference = FirebaseDatabase.getReference("database");
        mDatabaseReference = FirebaseDatabase.getInstance().getReference("database");
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

//        initArea();
        settingGeoFire();
        return root;
    }



//    private void initArea() {
//        geofences = new ArrayList<>();
//        geofences.add(new LatLng(-1.2926815360625805, 36.77455611526966));
//        geofences.add(new LatLng(-1.2922789718813648, 36.84138972312212));
//        geofences.add(new LatLng(-1.3093427, 36.8121728));
//
//        FirebaseDatabase.getInstance()
//                .getReference("database")
//                .child("geofence")
////                .push()
//                .setValue(geofences)
//                .addOnCompleteListener(new OnCompleteListener<Void>() {
//            @Override
//            public void onComplete(@NonNull Task<Void> task) {
//                Toast.makeText(getActivity(), "Geo-fence saved successfully", Toast.LENGTH_LONG).show();
//            }
//        });
//    }

    private void settingGeoFire() {
//        mDatabaseReference = FirebaseDatabase.getInstance().getReference("database");
        mGeoFire = new GeoFire(mDatabaseReference);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
//                mMap.clear(); //clear old markers

        CameraPosition cameraPosition = CameraPosition.builder()
                .target(new LatLng(-1.292671, 36.807081))
                .zoom(15)
                .bearing(0)
                .build();
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onMapClick(LatLng latLng) {
                MarkerOptions markerOptions = new MarkerOptions()
                                                    .position(latLng)
                                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
                Marker marker = mMap.addMarker(markerOptions);
                mLatLngs.add(latLng);
                mMarkers.add(marker);
                Log.d("Geo-fenceFragment", "Map clicked at [" + latLng.latitude + ", " + latLng.longitude + "]");
            }
        });
        displayAddedGeofences();
    }

    public void saveGeofence(LatLng latLng, int rad){
        mMap.addCircle(new CircleOptions().center(latLng)
                .radius(rad)
                .strokeColor(Color.BLUE)
                .fillColor(0x220000FF)
                .strokeWidth(5.0f));

        GeoQuery geoQuery = mGeoFire.queryAtLocation(new GeoLocation(latLng.latitude, latLng.longitude), 5.0);
        geoQuery.addGeoQueryEventListener(GeofenceFragment.this);

        FirebaseDatabase.getInstance()
                .getReference("database")
                .child("geofence")
                .push()
                .setValue(geofences)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(getActivity(), "Geo-fence saved successfully", Toast.LENGTH_LONG).show();
                    }
                });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            // Clear map of markers
            case R.id.clear_map:
                if (mPolygon != null)
                    mPolygon.remove();

                for (Marker marker : mMarkers)
                    marker.remove();

                mLatLngs.clear();
                mMarkers.clear();
                break;

             // Save points as geo-fence to firebase
            case R.id.addLocation:
                // button function
                if (mLatLngs.size() < 3) {
                    Toast.makeText(getActivity(), "Select at least 3 points on the map.", Toast.LENGTH_LONG).show();
                } else {
                    Objects.requireNonNull(mDialogSave.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                    mDialogSave.show();

                    if (mPolygon != null)
                        mPolygon.remove();

                    PolygonOptions polygonOptions = new PolygonOptions()
                            .addAll(mLatLngs)
                            .clickable(true);

                    mPolygon = mMap.addPolygon(polygonOptions);
                    mPolygon.setStrokeColor(Color.rgb(254, 240, 204));
                    mPolygon.setFillColor(Color.rgb(254, 240, 204));

                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mDialogSave.dismiss();
                        }
                    });

                    save.setOnClickListener(new View.OnClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                        @Override
                        public void onClick(View view) {
                            final String test = geofenceName.getText().toString().trim();
                            if (test.equals("")){
                                Toast.makeText(getActivity(), "Please enter the name of the geo-fence", Toast.LENGTH_LONG).show();
                            } else {
                                mDatabaseReference.child("geofences").addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.hasChild(test)){
                                            Toast.makeText(getActivity(), "A record with the name exists", Toast.LENGTH_LONG).show();
                                        } else {
                                            mDatabaseReference.child("geofences").child(test).setValue(mLatLngs);
                                            Toast.makeText(getActivity(), "Geo-fence saved successfully", Toast.LENGTH_LONG).show();

                                            if (mPolygon != null)
                                                mPolygon.remove();

                                            for (Marker marker : mMarkers)
                                                marker.remove();

                                            mLatLngs.clear();
                                            mMarkers.clear();
                                            mDialogSave.dismiss();

                                            mMap.clear();
                                            mPolygon.remove();
                                            geofenceName.setText("");
                                        }

//                                    displayAddedGeofences();
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }



                        }
                    });
                }
                break;

            // Delete geo-fence from firebase
            case R.id.deleteLocation:
//                button function
                Objects.requireNonNull(mDialogDelete.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                mDialogDelete.show();

                mCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDialogDelete.dismiss();
                    }
                });

                mDelete.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final String geofenceName = promptGeofenceName.getText().toString().trim();
                        if (geofenceName.equals("")){
                            Toast.makeText(getActivity(), "Please enter the name of the geo-fence", Toast.LENGTH_LONG).show();
                        } else {
                            mDatabaseReference.child("geofences").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    String[] splitRef = String.valueOf(dataSnapshot.getRef()).split("/");
                                    if (dataSnapshot.child(geofenceName).exists()){
                                        dataSnapshot.child(geofenceName).getRef().removeValue(new DatabaseReference.CompletionListener() {
                                            @Override
                                            public void onComplete(@Nullable DatabaseError databaseError, @NonNull DatabaseReference databaseReference) {
                                                Toast.makeText(getActivity(), "The record has been deleted", Toast.LENGTH_LONG).show();
                                                mMap.clear();
                                                mPolygon.remove();
                                                mDialogDelete.dismiss();
                                            displayAddedGeofences();
                                            }
                                        });
                                    } else {
                                        Toast.makeText(getActivity(), "No record of name " + dataSnapshot.child(geofenceName).getKey() + " exists.", Toast.LENGTH_LONG).show();
                                    }
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }

                    }
                });
                break;

            case R.id.map_type:
                switch (mMap.getMapType()){
                    case 1:
                        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                        break;

                    case 4:
                        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                        break;
                }
                break;
        }
    }

    private void displayAddedGeofences(){

        mDatabaseReference.child("geofences").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {

                    keys.add(postSnapshot.getKey());
                    Log.d("FETCHED DATA",postSnapshot.toString());

                    mDatabaseReference.child("geofences").child(String.valueOf(keys.get(geofenceLocation))).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot1) {
                            ArrayList<LatLng> latLngList = new ArrayList<>();
                            ArrayList<LatLng> markerLngList = new ArrayList<>();
                            Log.d("GOTTEN DATA", String.valueOf(dataSnapshot1.getRef()));
                            for (DataSnapshot postSnapshot1 : dataSnapshot1.getChildren()){

                                Log.d("GOTTEN DATA",postSnapshot1.getKey());
                                Log.d("GOTTEN DATA", String.valueOf(postSnapshot1.getValue()));
                                Log.d("GOTTEN DATA", String.valueOf(postSnapshot1.child("latitude").getValue()));
                                Log.d("GOTTEN DATA", String.valueOf(postSnapshot1.child("longitude").getValue()));
                                LatLng latLng = new LatLng(Double.parseDouble(postSnapshot1.child("latitude").getValue().toString()), Double.parseDouble(postSnapshot1.child("longitude").getValue().toString()));
                                latLngList.add(latLng);

                                if (String.valueOf(postSnapshot1.getKey()).equals("0")) {

                                    LatLng markerLng = new LatLng(Double.parseDouble(postSnapshot1.child("latitude").getValue().toString()), Double.parseDouble(postSnapshot1.child("longitude").getValue().toString()));
                                    markerLngList.add(markerLng);
                                    Log.d("MARKER DATA", String.valueOf(markerLngList));
                                }

                            }
                            Log.d("CONVERTED DATA", String.valueOf(dataSnapshot1.getRef()));
                            Log.d("CONVERTED DATA", String.valueOf(latLngList));

                            try {
                                PolygonOptions polygonOptions = new PolygonOptions()
                                        .addAll(latLngList)
                                        .clickable(true);

                                mPolygon = mMap.addPolygon(polygonOptions);
                                mPolygon.setStrokeColor(Color.argb(70, 249, 171, 49));
                                mPolygon.setFillColor(Color.argb(60, 249, 171, 49));
                                mPolygon.setClickable(true);
                                mPolygon.isClickable();

                                String[] splitRef = String.valueOf(dataSnapshot1.getRef()).split("/");
                                mMap.addMarker(new MarkerOptions()
//                                    .position(markerLngList.get(i))
                                        .position(getPolygonCenterPoint(latLngList))
                                        .title(splitRef[splitRef.length - 1])
                                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                            } catch (Exception e){
                                Log.e("latlngException", e.getMessage());
                            }

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                    geofenceLocation ++;
                }
                keys.clear();
                geofenceLocation = 0;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w("FETCHED DATA", "loadPost:onCancelled", databaseError.toException());
            }
        });
    }

    private void inGeofence(){

    }

    private LatLng getPolygonCenterPoint(ArrayList<LatLng> polygonPointsList){
        LatLng centerLatLng = null;
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for(int i = 0 ; i < polygonPointsList.size() ; i++)
        {
            builder.include(polygonPointsList.get(i));
        }
        LatLngBounds bounds = builder.build();
        centerLatLng =  bounds.getCenter();

        return centerLatLng;
    }

//    public void showPopup (View view) {
//        final TextInputEditText geofenceName;
//        CircularProgressButton cancel, save;
//
//        geofenceName = mDialogSave.findViewById(R.id.geofence_name);
//        cancel = mDialogSave.findViewById(R.id.cancel);
//        save = mDialogSave.findViewById(R.id.save);
//
//        cancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mDialogSave.dismiss();
//            }
//        });
//
//        save.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String test = geofenceName.getText().toString().trim();
//                mDatabaseReference.child(test).push().setValue(mLatLngs);
//                Toast.makeText(getActivity(), "Geo-fence saved successfully", Toast.LENGTH_LONG).show();
//
//                if (mPolygon != null)
//                    mPolygon.remove();
//
//                for (Marker marker : mMarkers)
//                    marker.remove();
//
//                mLatLngs.clear();
//                mMarkers.clear();
//                mDialogSave.dismiss();
//            }
//        });
//    }

    @Override
    public void onKeyEntered(String key, GeoLocation location) {
        sendNotification("SMPark", String.format("%s entered a geo-fence location", key));
    }

    @Override
    public void onKeyExited(String key) {
        sendNotification("EDMTDev", String.format("%s exited a geo-fence location", key));
    }

    @Override
    public void onKeyMoved(String key, GeoLocation location) {

    }

    @Override
    public void onGeoQueryReady() {

    }

    @Override
    public void onGeoQueryError(DatabaseError error) {
        Toast.makeText(getActivity(), " " + error.getMessage(), Toast.LENGTH_LONG).show();
    }

    private void sendNotification(String title, String content) {
        String NOTIFICATION_CHANNEL_ID = "edmt_multiple_location";
        NotificationManager notificationManager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notification", NotificationManager.IMPORTANCE_DEFAULT);

            notificationChannel.setDescription("Channel Description");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
            notificationChannel.enableVibration(true);
            notificationManager.createNotificationChannel(notificationChannel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(getActivity(), NOTIFICATION_CHANNEL_ID);
        builder.setContentTitle(title)
                .setContentText(content)
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher));

        Notification notification = builder.build();
        notificationManager.notify(new Random().nextInt(), notification);
    }

    @Override
    public void onPolygonClick(Polygon polygon) {

    }

    @Override
    public void onPolylineClick(Polyline polyline) {

    }
}