package com.example.projectadmin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;

public class LoginActivity extends AppCompatActivity {
    FirebaseAuth mFirebaseAuth;
    CircularProgressButton login;
//    EditText email, pass;
    TextInputEditText mEmail, mPass;
    TextView mForgotPass;
    ProgressBar mProgressBar;

    private FirebaseAuth.AuthStateListener mAuthStateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Get Firebase auth instance
        mFirebaseAuth = FirebaseAuth.getInstance();

        if (mFirebaseAuth.getCurrentUser() != null) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            finish();
        }

        setContentView(R.layout.activity_login);

        login = findViewById(R.id.login_login);
        mEmail = findViewById(R.id.email_login);
        mPass = findViewById(R.id.password_login);
        mProgressBar = findViewById(R.id.progressBar);
        mForgotPass = findViewById(R.id.forgot_password);

//        //Get Firebase auth instance
//        mFirebaseAuth = FirebaseAuth.getInstance();

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String email = mEmail.getText().toString().trim();
                final String password = mPass.getText().toString().trim();

                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.trim()).matches()) {
                    mEmail.setError("Enter a valid email");
                    mEmail.requestFocus();
                } if (TextUtils.isEmpty(password)) {
                    mPass.setError("Enter password");
                    mPass.requestFocus();
                }

                mProgressBar.setVisibility(View.VISIBLE);

                //authenticate user
                mFirebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        mProgressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()) {
                            FirebaseUser user = mFirebaseAuth.getCurrentUser();
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            // there was an error
                            Toast.makeText(LoginActivity.this, "Could not log in. Wrong credentials!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });

        mForgotPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });
    }
}
