package com.example.projectadmin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;

public class ForgotPasswordActivity extends AppCompatActivity {
    CircularProgressButton sendMail;
    TextInputEditText mEmail;
    ProgressBar mProgressBar;
    TextView mTextView;

    FirebaseAuth mFirebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        mTextView = findViewById(R.id.forgot_login);
        sendMail = findViewById(R.id.forgot_send);
        mEmail = findViewById(R.id.email_forgot);
        mProgressBar = findViewById(R.id.forgot_progressBar);

        mFirebaseAuth = FirebaseAuth.getInstance();

        mTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent login = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                startActivity(login);
            }
        });

        sendMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String email = mEmail.getText().toString().trim();

                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email.trim()).matches()) {
                    mEmail.setError("Enter a valid email");
                    mEmail.requestFocus();
                }
                mProgressBar.setVisibility(View.VISIBLE);
                mFirebaseAuth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        mProgressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()){
                            Toast.makeText(ForgotPasswordActivity.this, "Password sent to your Email", Toast.LENGTH_LONG).show();

                            Intent intent = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
                            startActivity(intent);
                        } else {
                            Toast.makeText(ForgotPasswordActivity.this, task.getException().getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }
}
